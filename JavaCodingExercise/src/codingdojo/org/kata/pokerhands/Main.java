package codingdojo.org.kata.pokerhands;

public class Main {

	public static void main(String[] args) {

		 String input = "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C AH";
//		 String input = "Black: 2H 4S 4C 2D 4H White: 2S 8S AS QS 3S";
//		 String input = "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C KH";
//		String input = "Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH";
		 
//		 String input = "Black: 2H 2D 4S 4C 5D  White: 2D 2H 4C 4S 6H";

		PokerHandsGame game = new PokerHandsGame(input);

		String result = game.getWinner();
		if (result.equals("Tie."))
			System.out.println(result);
		else
			System.out.println(result + " wins.");

	}

}
