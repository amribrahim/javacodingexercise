package codingdojo.org.kata.pokerhands;

import java.util.Arrays;

public class Hand {

	private Card[] cards;
	private String name;

	public Hand() {
		cards = null;
		name = "";
	}

	public Hand(String handString, String name) {
		String[] handCards = handString.split(" ");
		cards = new Card[handCards.length];
		for (int i = 0; i < cards.length; i++) {
			cards[i] = new Card(handCards[i]);
		}
		Arrays.sort(cards);
		this.name = name;
	}

	public Card[] getCards() {
		return this.cards;
	}

	public String getName() {
		return this.name;
	}

	
	HandRank getHandRank() {

		Card[] tempCards = this.getCards();

		if (isStraightFlush(tempCards))
			return HandRank.STRAIGHTFLUSH;
		else if (isFourOfAKind(tempCards))
			return HandRank.FOUROFAKIND;
		else if (isFullHouse(tempCards))
			return HandRank.FULLHOUSE;
		else if (isFlush(tempCards))
			return HandRank.FLUSH;
		else if (isStraight(tempCards))
			return HandRank.STRAIGHT;
		else if (isThreeOfAKind(tempCards))
			return HandRank.THREEOFAKIND;
		else if (isTwoPair(tempCards))
			return HandRank.TWOPAIR;
		else if (isPair(tempCards))
			return HandRank.PAIR;
		else
			return HandRank.HIGHCARD;
	}
	
	HandRankWithScore getHandRankWithScore() {

		Card[] tempCards = this.getCards();

		if (isStraightFlush(tempCards)) {
			Card tmpCard = this.getHighCard();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.STRAIGHTFLUSH, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isFourOfAKind(tempCards)) {
			Card tmpCard = this.getFourOfAKind();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.FOUROFAKIND, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isFullHouse(tempCards)) {
			Card tmpCard = this.getThreeOfAKind();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.FULLHOUSE, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isFlush(tempCards)) {
			Card tmpCard = this.getHighCard();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.FLUSH, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isStraight(tempCards)) {
			Card tmpCard = this.getHighCard();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.STRAIGHT, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isThreeOfAKind(tempCards)) {
			Card tmpCard = this.getThreeOfAKind();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.THREEOFAKIND, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isTwoPair(tempCards)) {
			Card tmpCard = this.getTwoPair();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.TWOPAIR, tmpCard.getValue());
			return handRankWithScore;
		}
		else if (isPair(tempCards)) {
			Card tmpCard = this.getPair();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.PAIR, tmpCard.getValue());
			return handRankWithScore;
		
		}
		else {
			Card tmpCard = this.getHighCard();
			HandRankWithScore handRankWithScore = new HandRankWithScore(HandRank.HIGHCARD, tmpCard.getValue());
			return handRankWithScore;
		}
	}
	
	

	private static boolean isStraightFlush(Card[] cards) {
		return isStraight(cards) && isFlush(cards);
	}

	//	Hand contains 5 cards with consecutive values
	private static boolean isStraight(Card[] cards) {
		// 2,3,4,5,6    
		// 10,11,12,13,14
		
		for(int i=0; i< cards.length-2; i++) {
			if( (cards[i+1].getValue() - cards[i].getValue()) != 1) {
				return false;
			}
		}
		return true;
		
	}

	//	Hand contains 5 cards of the same suit
	private static boolean isFlush(Card[] cards) {
		// J,J,J,J,J
		for (int i = 0; i < cards.length - 1; i++) {
			if (cards[i + 1].getSuit() != cards[i].getSuit())
				return false;
		}
		return true;
	}
	
	//	4 cards with the same value
	private static boolean isFourOfAKind(Card[] cards) {

		// 2,2,2,2,x    
		// x,2,2,2,2
		return (cards[0].getValue() == cards[3].getValue() || cards[1].getValue() == cards[4].getValue());
	}

	// 3 cards of the same value, with the remaining 2 cards forming a pair
	private static boolean isFullHouse(Card[] cards) {
		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();

		// 2,2,5,5,5    
		// 5,5,5,7,7
		return ((c1 == c2 && c2 != c3 && c3 == c5)
				|| (c1 == c3 && c3 != c4 && c4 == c5));
	}

	// Three of the cards in the hand have the same value.
	private static boolean isThreeOfAKind(Card[] cards) {
		// 3,3,3,4,5
		// 2,3,3,3,4
		// 2,3,4,4,4
		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();

		return (c1 == c3 || c2 == c4 || c3 == c5);
	}

	// The hand contains 2 different pairs.
	private static boolean isTwoPair(Card[] cards) {
		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();
		// 2,2,3,3,4
		// 2,3,3,4,4
		// 2,2,3,4,4
		return ((c1 == c2 && c3 == c4 && c2 != c3 && c4 != c5)
				|| (c1 != c2 && c2 == c3 && c3 != c4 && c4 == c5))
				|| (c1 == c2 && c2 != c3 && c3 != c4 && c4 == c5);
	}

	// 2 of the 5 cards in the hand have the same value.
	private static boolean isPair(Card[] cards) {
		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();

		// 2,2,3,4,5
		// 2,3,3,4,5
		// 2,3,4,4,5
		// 2,3,4,5,5
		return ((c1 == c2 && c2 != c3) || (c2 == c3 && c3 != c4 && c2 != c1)
				|| (c3 == c4 && c4 != c5 && c3 != c2) || (c4 == c5 && c4 != c3));
	}
	
	private Card getHighCard() {
		
		return cards[cards.length-1];
	}
	
	private Card getFourOfAKind() {
		
		if (cards[0].getValue() == cards[3].getValue())
			return cards[0];
		else if (cards[1].getValue() == cards[4].getValue())
			return cards[1];
		return null;
	}
	
	private Card getThreeOfAKind() {
		if (cards[0].getValue() == cards[2].getValue())
			return cards[0];
		else if (cards[1].getValue() == cards[3].getValue())
			return cards[1];
		else if (cards[2].getValue() == cards[4].getValue())
			return cards[2];
		return null;
	}
	
	private Card getTwoPair() {

		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();

		Card handBiggerPair = null;
		
		// XXYY-
		if (c1 == c2 && c3 == c4 && c2 != c3 && c4 != c5) {
			if (c1 > c3) {
				handBiggerPair = cards[0];
			} else {
				handBiggerPair = cards[2];
			}
		}
		// -XXYY
		if (c1 != c2 && c2 == c3 && c3 != c4 && c4 == c5) {
			if (c2 > c4) {
				handBiggerPair = cards[1];
			} else {
				handBiggerPair = cards[3];
			}
		}
		// XX-YY
		if (c1 == c2 && c4 == c5 && c2 != c3 && c4 != c3) {
			if (c1 > c4) {
				handBiggerPair = cards[0];
			} else {
				handBiggerPair = cards[3];
			}
		}
		return handBiggerPair;
		
	}
	
	private Card getPair() {
		int c1 = cards[0].getValue();
		int c2 = cards[1].getValue();
		int c3 = cards[2].getValue();
		int c4 = cards[3].getValue();
		int c5 = cards[4].getValue();
		
		Card handPair = null;

		// 2,2,3,4,5
		// 2,3,3,4,5
		// 2,3,4,4,5
		// 2,3,4,5,5
		if(c1 == c2 && c2 != c3) {
			handPair = cards[0];
		}
		if(c2 == c3 && c3 != c4 && c2 != c1){
			handPair = cards[1];
		}
		if(c3 == c4 && c4 != c5 && c3 != c2){
			handPair = cards[2];
		}
		if(c4 == c5 && c4 != c3){
			handPair = cards[3];
		}
		return handPair;
	}


}
