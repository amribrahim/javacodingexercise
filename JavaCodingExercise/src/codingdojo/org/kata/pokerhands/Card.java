package codingdojo.org.kata.pokerhands;

import java.util.HashMap;
import java.util.Map;

public class Card implements Comparable<Card> {

	private int value;
	private char suit;
	
	static Map<String, Integer> valuesMap = new HashMap<>();
	static { 
        valuesMap.put("2", 2);
        valuesMap.put("3", 3);
        valuesMap.put("4", 4);
        valuesMap.put("5", 5);
        valuesMap.put("6", 6);
        valuesMap.put("7", 7);
        valuesMap.put("8", 8);
        valuesMap.put("9", 9);
        valuesMap.put("T", 10);
        valuesMap.put("J", 11);
        valuesMap.put("Q", 12);
        valuesMap.put("K", 13);
        valuesMap.put("A", 14);
        
    } 
	
	public Card(String str) {
		value = valuesMap.get(str.substring(0, 1));
		suit = str.charAt(1);
	}

	public int getValue() {
		return value;
	}

	public char getSuit() {
		return suit;
	}

	@Override
	public int compareTo(Card anotherCard) {
		if (this.getValue() < anotherCard.getValue())
			return -1;
		else if (this.getValue() > anotherCard.getValue())
			return 1;
		else
			return 0;
	}

	@Override
	public String toString() {
		return "[" + value + suit + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (value != other.value)
			return false;
		return true;
	}

}
