package codingdojo.org.kata.pokerhands;

public class HandRankWithScore {
	
	HandRank handRank;
	int score;
	
	int compare;
	
	public HandRankWithScore() {
		// TODO Auto-generated constructor stub
	}
	
	public HandRankWithScore(HandRank handRank, int score) {
		super();
		this.handRank = handRank;
		this.score = score;
	}

	public HandRank getHandRank() {
		return handRank;
	}
	public void setHandRank(HandRank handRank) {
		this.handRank = handRank;
	}

	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public int getCompare() {
		return compare;
	}
	public void setCompare(int compare) {
		this.compare = compare;
	}
	

}
