package codingdojo.org.kata.pokerhands;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PokerHandsGame {

	Hand hand1;
	Hand hand2;

	public PokerHandsGame(String input) {

		int blackStartIndex = input.indexOf("Black") + 7;
		int whiteStartIndex = input.indexOf("White") + 7;
		String blackHandString = input.substring(blackStartIndex, blackStartIndex + 14);
		String whiteHandString = input.substring(whiteStartIndex, input.length());

		System.out.println(blackHandString);
		System.out.println(whiteHandString);

		hand1 = new Hand(blackHandString, "Black");
		hand2 = new Hand(whiteHandString, "White");
	}

	String getWinner() {

		HandRankWithScore hand1Rank = hand1.getHandRankWithScore();
		HandRankWithScore hand2Rank = hand2.getHandRankWithScore();
		
		int cmp = hand1Rank.getHandRank().compareTo(hand2Rank.getHandRank());
		if (cmp > 0)
			return hand1.getName() + " with " + hand1Rank.getHandRank() + ": " + hand1Rank.getScore();
		else if (cmp < 0)
			return hand2.getName() + " with " + hand2Rank.getHandRank() + ": " + hand2Rank.getScore();
		else {
			
			switch (hand1Rank.getHandRank()) {
			case STRAIGHTFLUSH:
				cmp = compareHighCard(hand1, hand2);
				break;
			case FOUROFAKIND:
				cmp = compareFourOfAKind(hand1).compareTo(compareFourOfAKind(hand2));
				break;
			case FULLHOUSE:
				cmp = compareThreeOfAKind(hand1).compareTo(compareThreeOfAKind(hand2));
				break;
			case FLUSH:
				cmp = compareHighCard(hand1, hand2);
				break;
			case STRAIGHT:
				cmp = compareHighCard(hand1, hand2);
				break;
			case THREEOFAKIND:
				cmp = compareThreeOfAKind(hand1).compareTo(compareThreeOfAKind(hand2));
				break;
			case TWOPAIR:
				cmp = compareTwoPair(hand1, hand2);
				break;
			case PAIR:
				cmp = comparePair(hand1, hand2);
				break;
			case HIGHCARD:
				cmp = compareHighCard(hand1, hand2);
				break;
			}
		}

		return cmp > 0 ? hand1.getName() + " with " + hand1.getHandRank() 
				: cmp < 0 ? hand2.getName() + " with " + hand2.getHandRank() : "Tie.";
	}

	private int compareHighCard(Hand hand1, Hand hand2) {
		Card[] hand1Cards = hand1.getCards();
		Card[] hand2Cards = hand2.getCards();
		int current = hand1Cards.length - 1;
		int cmp = 0;
		while (current >= 0) {
			cmp = hand1Cards[current].compareTo(hand2Cards[current]);
			if (cmp != 0)
				break;
			current--;
		}
		return cmp;
	}
	
	

	private Card compareFourOfAKind(Hand tempHand) {

		Card[] tempCards = tempHand.getCards();
		if (tempCards[0] == tempCards[3])
			return tempCards[0];
		else if (tempCards[1] == tempCards[4])
			return tempCards[1];
		return null;
	}

	// 01234
	// xxx..
	// .xxx.
	// ..xxx
	private Card compareThreeOfAKind(Hand tempHand) {
		Card[] tempCards = tempHand.getCards();
		if (tempCards[0] == tempCards[2])
			return tempCards[0];
		else if (tempCards[1] == tempCards[3])
			return tempCards[1];
		else if (tempCards[2] == tempCards[4])
			return tempCards[2];
		return null;
	}

	// XXYY-
	// -XXYY
	// XX-YY
	private int compareTwoPair(Hand hand1, Hand hand2) {

		Card[] hand1Cards = hand1.getCards();

		int c1 = hand1Cards[0].getValue();
		int c2 = hand1Cards[1].getValue();
		int c3 = hand1Cards[2].getValue();
		int c4 = hand1Cards[3].getValue();
		int c5 = hand1Cards[4].getValue();

		Card hand1Bigger = null;
		Card hand1Smaller = null;
		Card hand1Remain = null;
		// XXYY-
		if (c1 == c2 && c3 == c4 && c2 != c3 && c4 != c5) {
			if (c1 > c3) {
				hand1Bigger = hand1Cards[0];
				hand1Smaller = hand1Cards[2];
				hand1Remain = hand1Cards[4];
			} else {
				hand1Bigger = hand1Cards[2];
				hand1Smaller = hand1Cards[0];
				hand1Remain = hand1Cards[4];
			}
		}
		// -XXYY
		if (c1 != c2 && c2 == c3 && c3 != c4 && c4 == c5) {
			if (c2 > c4) {
				hand1Bigger = hand1Cards[1];
				hand1Smaller = hand1Cards[3];
				hand1Remain = hand1Cards[0];
			} else {
				hand1Bigger = hand1Cards[3];
				hand1Smaller = hand1Cards[1];
				hand1Remain = hand1Cards[0];
			}
		}
		// XX-YY
		if (c1 == c2 && c4 == c5 && c2 != c3 && c4 != c3) {
			if (c1 > c4) {
				hand1Bigger = hand1Cards[0];
				hand1Smaller = hand1Cards[3];
				hand1Remain = hand1Cards[2];
			} else {
				hand1Bigger = hand1Cards[3];
				hand1Smaller = hand1Cards[0];
				hand1Remain = hand1Cards[2];
			}
		}
		Card[] hand2Cards = hand2.getCards();
		int c1_h2 = hand2Cards[0].getValue();
		int c2_h2 = hand2Cards[1].getValue();
		int c3_h2 = hand2Cards[2].getValue();
		int c4_h2 = hand2Cards[3].getValue();
		int c5_h2 = hand2Cards[4].getValue();

		Card hand2Bigger = null;
		Card hand2Smaller = null;
		Card hand2Remain = null;

		// XXYY-
		if (c1_h2 == c2_h2 && c3_h2 == c4_h2 && c2_h2 != c3_h2 && c4_h2 != c5_h2) {
			if (c1_h2 > c3_h2) {
				hand2Bigger = hand2Cards[0];
				hand2Smaller = hand2Cards[2];
				hand2Remain = hand2Cards[4];
			} else {
				hand2Bigger = hand2Cards[2];
				hand2Smaller = hand2Cards[0];
				hand2Remain = hand2Cards[4];
			}
		}
		// -XXYY
		if (c1_h2 != c2_h2 && c2_h2 == c3_h2 && c3_h2 != c4_h2 && c4_h2 == c5_h2) {
			if (c2_h2 > c4_h2) {
				hand2Bigger = hand2Cards[1];
				hand2Smaller = hand2Cards[3];
				hand2Remain = hand2Cards[0];
			} else {
				hand2Bigger = hand2Cards[3];
				hand2Smaller = hand2Cards[1];
				hand2Remain = hand2Cards[0];
			}
		}
		// XX-YY
		if (c1_h2 == c2_h2 && c4_h2 == c5_h2 && c2_h2 != c3_h2 && c4_h2 != c3_h2) {
			if (c1_h2 > c4_h2) {
				hand2Bigger = hand2Cards[0];
				hand2Smaller = hand2Cards[3];
				hand2Remain = hand2Cards[2];
			} else {
				hand2Bigger = hand2Cards[3];
				hand2Smaller = hand2Cards[0];
				hand2Remain = hand2Cards[2];
			}
		}

		if (hand1Bigger.compareTo(hand2Bigger) != 0)
			return hand1Bigger.compareTo(hand2Bigger);
		else if (hand1Smaller.compareTo(hand2Smaller) != 0)
			return hand1Smaller.compareTo(hand2Smaller);
		else
			return hand1Remain.compareTo(hand2Remain);
	}

	private int comparePair(Hand hand1, Hand hand2) {

		Card[] hand1Cards = hand1.getCards();
		Card[] hand2Cards = hand2.getCards();

		Set<Card> hand1Set = new HashSet<Card>();
		Set<Card> hand2Set = new HashSet<Card>();

		Card hand1PairCard = null;
		Card hand2PairCard = null;

		for (int i = 0; i < hand1Cards.length; i++) {
			if (!hand1Set.contains(hand1Cards[i]))
				hand1Set.add(hand1Cards[i]);
			else {
				hand1PairCard = hand1Cards[i];
				hand1Set.remove(hand1Cards[i]);
			}
		}
		for (int i = 0; i < hand2Cards.length; i++) {
			if (!hand2Set.contains(hand2Cards[i]))
				hand2Set.add(hand2Cards[i]);
			else {
				hand2PairCard = hand2Cards[i];
				hand2Set.remove(hand2Cards[i]);
			}
		}

		if (hand1PairCard.compareTo(hand2PairCard) != 0)
			return hand1PairCard.compareTo(hand2PairCard);
		else {
			while (!hand1Set.isEmpty() && !hand2Set.isEmpty()) {
				Card tempHand1Max = Collections.max(hand1Set);
				Card tempHand2Max = Collections.max(hand2Set);
				if (tempHand1Max.compareTo(tempHand2Max) != 0)
					return tempHand1Max.compareTo(tempHand2Max);
				hand1Set.remove(tempHand1Max);
				hand2Set.remove(tempHand2Max);
			}
		}
		return 0;
	}

}
